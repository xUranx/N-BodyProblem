#include "Camera3D.h"
#include <glm/gtc/matrix_transform.hpp>
namespace Engine
{


Camera3D::Camera3D()
{
}


Camera3D::~Camera3D()
{
}

void Camera3D::init(float fov, float aspect, float zNear, float zFar)
{
	m_perspectiveMatrix = glm::perspective(glm::radians(fov), aspect, zNear, zFar);
	m_dir = glm::vec3(
		glm::cos(angle.y) * glm::sin(angle.x),
		glm::sin(angle.y),
		glm::cos(angle.y) * glm::cos(angle.x)
	);

	// Right vector
	m_right = glm::vec3(
		glm::sin(angle.x - 3.14f / 2.0f),
		0,
		glm::cos(angle.x - 3.14f / 2.0f)
	);
}
void Camera3D::update()
{
	if (m_needsUpdate)
	{

		m_dir = glm::vec3(
			glm::cos(angle.y) * glm::sin(angle.x),
			glm::sin(angle.y),
			glm::cos(angle.y) * glm::cos(angle.x)
		);

		// Right vector
		m_right = glm::vec3(
			glm::sin(angle.x - 3.14f / 2.0f),
			0,
			glm::cos(angle.x - 3.14f / 2.0f)
		);


		glm::vec3 up = glm::cross(m_right, m_dir);

		m_viewMat = glm::lookAt(m_pos, m_pos + m_dir, up);
		m_needsUpdate = false;
	}
}
}