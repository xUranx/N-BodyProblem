#include "MainGame.h"
#include "Log.h"
#include <iostream>
using namespace Engine;
MainGame::MainGame(): sWidth(1024), sHeight(640)
{
}


MainGame::~MainGame()
{
}


void MainGame::run()
{
	if (!window.init("NBodyProblem", sWidth, sHeight, 0))
	{
		fatal_error("Failed to init window");
	}
	else
	{	
		
		glClearColor(0.0f / 255.0f, 0.0f / 255.0f, 30.0f / 255.0f, 1.0f);
		glClearDepth(1.0);
		glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
		glDepthFunc(GL_LESS);    // Set the type of depth-test
		
		SDL_ShowCursor(SDL_DISABLE);
		SDL_SetRelativeMouseMode(SDL_TRUE);

		nb.Init(3000);//Inits Simulation

		gLoop();
	}

	window.close();//Free resources and close SDL
}


void MainGame::gLoop()
{
	
	//Main loop flag
	bool quit = false;

	SDL_Event e;


	while (!quit)
	{
		window.fpsCounter();
	
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{	
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			if (e.type == SDL_KEYDOWN)
			{
				inputManager.pressKey(e.key.keysym.sym);
			}
			if (e.type == SDL_KEYUP)
			{
				inputManager.releaseKey(e.key.keysym.sym);
			}
			if (e.type == SDL_MOUSEMOTION)
			{
				Engine::Camera3D* cam = nb.getCamera();
				int x, y;
				cam->rotateCamera(0.002f, { -e.motion.xrel, -e.motion.yrel });
				//SDL_WarpMouseInWindow(window.gWindow, sWidth/2, sHeight/2);
			}

		}
		processInput();
		if (inputManager.isKeyPressed(SDLK_ESCAPE)) quit = true;

		nb.simulate();
		drawGame();

	}
}

void MainGame::processInput()
{
	const float CamSpeed = 2.0f;
	const float ScalSpeed = 0.2f;
	Engine::Camera3D* cam = nb.getCamera();
	if (inputManager.isKeyPressed(SDLK_w))
		cam->moveForward(CamSpeed);
	if (inputManager.isKeyPressed(SDLK_s))
		cam->moveForward(-CamSpeed);
	if (inputManager.isKeyPressed(SDLK_d))
		cam->moveRight(CamSpeed);
	if (inputManager.isKeyPressed(SDLK_a))
		cam->moveRight(-CamSpeed);
}

void MainGame::drawGame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	nb.draw();

	SDL_GL_SwapWindow(window.gWindow);
}