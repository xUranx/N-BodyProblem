#pragma once
#include "Window.h"

#include "InputManager.h"
#include "NBodyProblem.h"

class MainGame
{
public:
	MainGame();
	~MainGame();
	void run();
private:

	int sWidth, sHeight;
	void gLoop();
	void drawGame();
	void processInput();

	Engine::Window window;

	Engine::InputManager inputManager;
	NBodyProblem nb;
};

