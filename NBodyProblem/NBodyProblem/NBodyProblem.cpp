#include "NBodyProblem.h"
#include <algorithm>
#include "loadImage.h"
#include <math.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
#include "Log.h"
#include <time.h>
#include <random>

float NodeTresh = 0.7f;

float delta = 1.0f/60.f;
//float delta = 0.06;

//int LastUsedParticle = 0;

float G = 0.00001;
float const Softener = 10;

bool DrawTree = false;
bool DrawTrail = true;

Node::~Node()
{
	if (!child.empty())
	{
		for (int i = 0; i < child.size();i++)
		{
			delete child[i];
		}
		child.clear();
	}
}

bool Node::createNode(glm::vec3 npos, glm::vec3 nrect, Particle* body)
{
	pos = npos;
	rect = nrect;
	if (posCheck(body))
	{
		center = *body;
		return true;
	}
	return false;
}

bool Node::generateChilds(Particle* body)
{
	if (posCheck(body) && child.empty() && center.parent == nullptr)
	{
		center = *body;
		center.parent = this;
		return true;
	}
	else if (posCheck(body))
	{
		std::vector<Offset> offs;
		if (!child.empty())
		{
			for (auto c : child)
			{
				if (c->posCheck(body))
					if (c->generateChilds(body))
						return true;
				offs.push_back(c->m_offset);
			}
			for (int i = 1; i < 9; i++)
			{
				if (std::find(offs.begin(), offs.end(), i) != offs.end())
				{
					continue;
				}
				glm::vec3 off = offset(i);
				off = pos + off;
				Node* nod = new Node;
				bool in = nod->createNode(off, rect*0.5f, body);
				if (in)
				{
					nod->parent = this;
					nod->generateChilds(body);
					child.push_back(nod);
					return true;
				}
				delete nod;

			}

		}
		else
		{
			for (int i = 1; i < 9; i++)
			{
				glm::vec3 off = offset(i);
				off = pos + off;
				Node* nod = new Node;
				bool in = nod->createNode(off, rect*0.5f, body);
				if (in)
				{
					nod->parent = this;
					nod->generateChilds(body);
					child.push_back(nod);
					center.parent = nullptr;
					generateChilds(&center);
					return true;
				}
				delete nod;

			}
		}
	}
	else
		return false;
	
}

std::vector<Particle*> Node::checkNode(Particle* body)
{
	float dist = glm::length2(center.pos - body->pos);
	float w = rect.x*rect.x;
	std::vector<Particle*> bodys;
	if (w / dist < NodeTresh)
		bodys.push_back(&center);
	else
	{
		if (!child.empty())
		{
			for (auto c : child)
			{
				c->checkNode(body, bodys);
			}
		}
		else
		{
			bodys.push_back(&center);
		}
	}
	return bodys;
}

void Node::checkNode(Particle* body, std::vector<Particle*> &bodys)
{
	float dist = glm::length2(center.pos - body->pos);
	float w = rect.x*rect.x;
	if (dist == 0)
	{
		//Engine::Message("Dist == 0");
		return;
	}
	if (w / dist < NodeTresh)
		bodys.push_back(&center);
	else
	{
		if (!child.empty())
		{
			for (auto c : child)
			{
				c->checkNode(body, bodys);
			}
		}
		else
		{
			bodys.push_back(&center);
		}
	}
}

void Node::calculateCenterOfMass()
{
	if (child.empty())
		return;
	
	center.mass = 0;
	glm::vec3 pos(0.0f);

	for (auto c = child.begin(); c != child.end(); c++)
	{
		(*c)->calculateCenterOfMass();
		center.mass += (*c)->center.mass;
		pos += (*c)->center.pos*(*c)->center.mass;
	}
	center.pos = pos * (1 / center.mass);
}

void Node::draw(Engine::DebugRenderer & dr)
{
	dr.drawCube(pos, rect, {0,255,0,255});
	if(!child.empty())
	{
		for (auto c : child)
		{
			c->draw(dr);
		}
	}
}

glm::vec3 Node::offset(int off)
{
	switch (off)
	{
	case None:
		return{ 0,0,0 };
		break;
	case TLF:
		return{-rect.x / 4,rect.y / 4,rect.z / 4};
		break;
	case BLF:
		return{ -rect.x / 4,-rect.y / 4,rect.z / 4 };
		break;
	case BRF:
		return{ rect.x / 4,-rect.y / 4,rect.z / 4 };
		break;
	case TRF:
		return{ rect.x / 4,rect.y / 4,rect.z / 4 };
		break;
	case TLB:
		return{ -rect.x / 4,rect.y / 4,-rect.z / 4 };
		break;
	case BLB:
		return{ -rect.x / 4,-rect.y / 4,-rect.z / 4 };
		break;
	case BRB:
		return{ rect.x / 4,-rect.y / 4,-rect.z / 4 };
		break;
	case TRB:
		return{ rect.x / 4,rect.y / 4,-rect.z / 4 };
		break;
	default:
		break;
	}
	return glm::vec3();
}

bool Node::posCheck(Particle* body)
{
	if ((pos.x - rect.x * 0.5f <= body->pos.x && pos.x + rect.x * 0.5f >= body->pos.x)
	 && (pos.y - rect.y * 0.5f <= body->pos.y && pos.y + rect.y * 0.5f >= body->pos.y)
	 && (pos.z - rect.z * 0.5f <= body->pos.z && pos.z + rect.z * 0.5f >= body->pos.z))
		return true;
	else
		return false;
}



NBodyProblem::NBodyProblem()
{
}


NBodyProblem::~NBodyProblem()
{
	delete m_ps;
	for (int i = 0; i < bhut.child.size();i++)
	{
		delete bhut.child.at(i);
	}
	bhut.child.clear();
}

void NBodyProblem::Init(int PCout)
{
	G = 6.67384*std::powf(10, -20);
	//MaxParticles = PCout;
	//m_bodys.resize(PCout);
	m_bodys.resize(5);

	m_glsl.compileShaders("Assets/Shaders/particle.vert", "assets/Shaders/particle.frag");
	m_glsl.addAtribute("squareVertices");
	m_glsl.addAtribute("xyzs");
	m_glsl.addAtribute("color");
	m_glsl.linkShaders();

	m_cam.init(45.0f, 1024.0f / 640.0f, 0.1f, 1000.0f);
	m_cam.setPos({ 0.0f,0.0f, 400.0f });
	m_ps = new Engine::ParticleSystem;
	m_ps->init(PCout);

	debug.init();

	bhut.createNode({ 0,0,0 }, { 800,800,800}, &m_bodys[0]);

	textureID = loadDDS("Assets/Textures/particle.DDS");

	int trail_MaxSize = 40;

	std::mt19937 randGene(time(NULL));
	std::uniform_real_distribution<float> xPos(-50.0f, 50.0f);
	std::uniform_real_distribution<float> yPos(-50.0f, 50.0f);
	std::uniform_real_distribution<float> zPos(-50.0f, 50.0f);
	std::uniform_real_distribution<float> xS(-1.2f, 1.2f);
	std::uniform_real_distribution<float> yS(-1.2f, 1.2f);
	std::uniform_real_distribution<float> zS(-1.2f, 1.2f);
	std::uniform_real_distribution<float> mass(1000000000.0f,5000000000.0f);

	m_bodys[0].size = 14.0f;
	m_bodys[0].pos = { 0,0,0 };
	m_bodys[0].speed = { 0,0,0 };
	m_bodys[0].mass = 5.97237*std::powf(10,24);
	m_bodys[0].collor.setColour(0, 70, 200, 180);
	m_bodys[0].move = false;

	m_bodys[1].pos = {100.0f,0,0 };
	m_bodys[1].size = 8.0f;
	m_bodys[1].speed = { 0,63.13f,0 };
	m_bodys[1].mass = 1;
	m_bodys[1].collor.setColour(255, 0, 0, 100);
	m_bodys[1].trailMaxSize = trail_MaxSize;

	m_bodys[2].pos = { -100.0f,0,0 };
	m_bodys[2].size = 8.0f;
	m_bodys[2].speed = { 0,-63.13f,0 };
	m_bodys[2].mass = 1;
	m_bodys[2].collor.setColour(0, 200, 0, 100);
	m_bodys[2].trailMaxSize = trail_MaxSize;

	m_bodys[3].pos = { 0,100.0f,0 };
	m_bodys[3].size = 8.0f;
	m_bodys[3].speed = { -63.13f,0,0 };
	m_bodys[3].mass = 1;
	m_bodys[3].collor.setColour(0, 0, 255, 100);
	m_bodys[3].trailMaxSize = trail_MaxSize;

	m_bodys[4].pos = { 0,-100.0f,0 };
	m_bodys[4].size = 8.0f;
	m_bodys[4].speed = { 63.13f,0,0 };
	m_bodys[4].mass = 1;
	m_bodys[4].collor.setColour(150, 100, 50, 100);
	m_bodys[4].trailMaxSize = trail_MaxSize;

	/*for (auto i = m_bodys.begin()+1; i != m_bodys.end();i++)
	{
		(*i).pos = { xPos(randGene),yPos(randGene), zPos(randGene) };
		(*i).size = 1.3f;
		(*i).speed = { xS(randGene),yS(randGene),zS(randGene)};
		(*i).mass = mass(randGene);
		(*i).collor.setColour(200, 200, 200, 200);
		(*i).trailMaxSize = 120;
		(*i).trail.resize((*i).trailMaxSize);
	}*/
	//bhut.calculateCenterOfMass();
}

glm::vec3 NBodyProblem::gravity(glm::vec3 vector, float mass)
{
	float r = glm::length2(vector);
	r = r * r*r;
	r = 1 / r;
	float accel = mass * r * G;
	return vector * accel;
}

int skipFrames = 2;
int framesSkipped = 0;

void NBodyProblem::simulate()
{
	for (int i = 0; i < bhut.child.size();i++)
	{
		delete bhut.child.at(i);
	}
	bhut.child.clear();
	for (auto &c : m_bodys)
	{
		bhut.generateChilds(&c);
		c.dpos = c.pos + c.speed*(delta / 2);
	}
	bhut.calculateCenterOfMass();

	for (auto i = m_bodys.begin(); i != m_bodys.end(); i++)
	{
		std::vector<Particle*> bods = bhut.checkNode(&(*i));
		if ((*i).move == true)
		{
			(*i).nspeed = glm::vec3(0);
			for (auto j = bods.begin(); j != bods.end(); j++)
			{
				glm::vec3 vector = (*i).dpos - (*j)->dpos;
				float r = glm::length(vector);
				float accel = -G*((*j)->mass /(r*r));
				(*i).nspeed += (accel/r)*vector;

			}
			(*i).speed += (*i).nspeed*delta;

		}
		
	}
	for (auto i = m_bodys.begin(); i != m_bodys.end(); i++)
	{
		if ((*i).move == true)
		{
			(*i).pos = (*i).dpos + (*i).speed*(delta/2);
			(*i).cameraDistance = glm::length2((*i).pos - m_cam.getPos());

			if ((framesSkipped >= skipFrames) && DrawTrail)
			{
				(*i).calcTrail();
				framesSkipped = 0;
			}
			else if(DrawTrail)
				framesSkipped++;
		}
	}
}

void NBodyProblem::draw()
{

	m_cam.update();

	m_glsl.use();

	m_ps->begin();

	sortParticles();

	glActiveTexture(GL_TEXTURE0);
	GLint textLoc = m_glsl.getUniformLoc("Textu");
	glBindTexture(GL_TEXTURE_2D, textureID);
	glUniform1i(textLoc, 0);


	GLint pLoc = m_glsl.getUniformLoc("viewMat");

	glm::mat4 camMat = m_cam.getCameraMatrix();
	glUniformMatrix4fv(pLoc, 1, GL_FALSE, &(camMat[0][0]));

	GLuint CameraRight_worldspace_ID = m_glsl.getUniformLoc("CameraRight");
	GLuint CameraUp_worldspace_ID = m_glsl.getUniformLoc("CameraUp");

	glm::mat4 ViewMatrix = m_cam.getViewMat();
	glUniform3f(CameraRight_worldspace_ID, ViewMatrix[0][0], ViewMatrix[1][0], ViewMatrix[2][0]);
	glUniform3f(CameraUp_worldspace_ID, ViewMatrix[0][1], ViewMatrix[1][1], ViewMatrix[2][1]);
	for (auto c : m_bodys)
	{
		m_ps->draw({ c.pos, c.size }, c.collor);
	}
	//m_ps->draw({ 0,0,-20,0.6f }, Engine::ColourRGBA8(200, 200, 200, 100.f));
	m_ps->end();

	m_ps->render();

	glBindTexture(GL_TEXTURE_2D, 0);
	m_glsl.unuse();

	//debug.drawLine({ 0,0,-20 }, { 5,0,-20 }, Engine::ColourRGBA8(255, 255, 0, 200));

	//debug.drawCube({ 0,0,-20 }, { 20,20,20 }, Engine::ColourRGBA8(0, 200, 0, 200));

	if(DrawTree)
		bhut.draw(debug);
	if (DrawTrail)
	{
		for (auto c : m_bodys)
		{
			c.draw(debug);
		}
	}

	debug.end();

	debug.render(camMat, 1);

}


//private
void NBodyProblem::sortParticles()
{
	std::sort(m_bodys.begin(), m_bodys.end());
}

