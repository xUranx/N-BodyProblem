#pragma once
#include <vector>
#include "ParticleSystem.h"
#include "Particle.h"
#include "Camera3D.h"
#include "GLSLProgram.h"
#include "DebugRenderer.h"


enum Offset
{
	None = 0, 
	TLF, BLF, BRF, TRF,
	TLB, BLB, BRB, TRB
};

class Node
{
public:
	Node() {}

	~Node();

	std::vector<Node*> child;
	Particle center;

	Node* parent = nullptr;

	bool createNode(glm::vec3 npos, glm::vec3 nrect, Particle* body);

	bool generateChilds(Particle* body);

	std::vector<Particle*> checkNode(Particle* body);
	void checkNode(Particle* body, std::vector<Particle*> &bodys);

	void calculateCenterOfMass();

	Offset m_offset = None;

	void draw(Engine::DebugRenderer& dr);
	bool posCheck(Particle* body);
private:
	glm::vec3 offset(int off);

	glm::vec3 pos;
	glm::vec3 rect;
};

class NBodyProblem
{
public:
	NBodyProblem();
	~NBodyProblem();

	void Init(int PCout);
	void simulate();
	void draw();
	Engine::Camera3D* getCamera() { return &m_cam; }
private:
	glm::vec3 gravity(glm::vec3 vector, float mass);
	void sortParticles();
	Node bhut;
	
	int MaxParticles;
	std::vector<Particle> m_bodys;
	Engine::ParticleSystem* m_ps;
	Engine::GLSLProgram m_glsl;
	Engine::Camera3D m_cam;
	Engine::DebugRenderer debug;
	GLuint textureID;
};

