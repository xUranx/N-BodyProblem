#pragma once
#include <glm\glm.hpp>
#include "Vertex.h"
#include "ParticleSystem.h"
#include "DebugRenderer.h"

class Node;

struct Particle
{
	glm::vec3 pos,dpos, speed, nspeed;
	Engine::ColourRGBA8 collor;
	std::vector<glm::vec3> trail;
	float size;
	float mass;
	bool force = false;
	bool move = true;
	int trailSize = 0;
	int trailMaxSize;
	float cameraDistance;

	Node* parent;

	void draw(Engine::DebugRenderer& dr)
	{
		if (trail.size() > 2)
		{
			for (int i = 0; i < trail.size() - 1; i++)
			{
				dr.drawLine(trail[i], trail[i + 1], { 255,255,255,255 });
			}
		}
	}

	void calcTrail()
	{
		if (trail.size() >= trailMaxSize)
		{
			trail.erase(trail.begin());
			trail.push_back(pos);
		}
		else
			trail.push_back(pos);
	}

	bool operator<(Particle& that)
	{
		return this->cameraDistance > that.cameraDistance;
	}

	bool operator==(Particle& that)
	{

		if (pos == that.pos && collor == that.collor && size == that.size && force == that.force && move == that.move)
			return true;
		else
			return false;
	}
	
};