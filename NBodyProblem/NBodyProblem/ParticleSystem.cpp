#include "ParticleSystem.h"

namespace Engine
{

	ParticleSystem::ParticleSystem()
	{
	}


	ParticleSystem::~ParticleSystem()
	{
		glDeleteBuffers(1, &b_vbo);
		glDeleteBuffers(1, &p_vbo);
		glDeleteBuffers(1, &c_vbo);

		glVertexAttribDivisor(0, 0);
		glVertexAttribDivisor(1, 0);
		glVertexAttribDivisor(2, 0);

		glDeleteVertexArrays(1, &vao);
	}

	void ParticleSystem::init(int MaxParticles)
	{
		m_maxParticles = MaxParticles;
		static const GLfloat g_vertex_buffer_data[] = {
			-0.5f, -0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			-0.5f, 0.5f, 0.0f,
			0.5f, 0.5f, 0.0f,
		};


		if (vao == 0)
		{
			glGenVertexArrays(1, &vao);
		}

		glBindVertexArray(vao);

		if (b_vbo == 0)
		{
			glGenBuffers(1, &b_vbo);
		}
		glBindBuffer(GL_ARRAY_BUFFER, b_vbo);
		
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

		if (p_vbo == 0)
		{
			glGenBuffers(1, &p_vbo);
		}
		glBindBuffer(GL_ARRAY_BUFFER, p_vbo);
		//glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(Posit3D), NULL, GL_STREAM_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Posit3D), (void*)0);

		if (c_vbo == 0)
		{
			glGenBuffers(1, &c_vbo);
		}
		glBindBuffer(GL_ARRAY_BUFFER, c_vbo);
		glEnableVertexAttribArray(2);
		//glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(ColourRGBA8), NULL, GL_STREAM_DRAW);
		glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ColourRGBA8), (void*)0);
		
		glVertexAttribDivisor(0, 0); // particles vertices : always reuse the same 4 vertices -> 0
		glVertexAttribDivisor(1, 1); // positions : one per quad (its center) -> 1
		glVertexAttribDivisor(2, 1); // color : one per quad -> 1

		glBindVertexArray(0);
	}

	void ParticleSystem::begin()
	{
		m_ppb.clear();
		m_color.clear();
	}

	void ParticleSystem::draw(glm::vec4 destRect, ColourRGBA8 color)
	{
		m_ppb.emplace_back(destRect);
		m_color.push_back(color);
	}

	void ParticleSystem::end()
	{
		glBindBuffer(GL_ARRAY_BUFFER, p_vbo);
		glBufferData(GL_ARRAY_BUFFER, m_ppb.size() * sizeof(Posit3D), NULL, GL_STREAM_DRAW); // Buffer orphaning
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_ppb.size() * sizeof(Posit3D), m_ppb.data());
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, c_vbo);
		glBufferData(GL_ARRAY_BUFFER, m_color.size() * sizeof(ColourRGBA8), NULL, GL_STREAM_DRAW); // Buffer orphaning
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_color.size() * sizeof(ColourRGBA8), m_color.data());
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void ParticleSystem::render()
	{
		glBindVertexArray(vao);

		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, m_ppb.size());

		glBindVertexArray(0);
	}

}
