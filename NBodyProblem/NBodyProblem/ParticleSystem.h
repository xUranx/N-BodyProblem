#pragma once
#include <glew\glew.h>
#include <glm\glm.hpp>
#include "Vertex.h"
#include <vector>
namespace Engine
{

	struct Posit3D
	{
		Posit3D(glm::vec4 destRect)
		{
			x = destRect.x;
			y = destRect.y;
			z = destRect.z;
			size = destRect.w;
		}
		float x, y, z, size;
	};

	class ParticleSystem
	{
	public:
		ParticleSystem();
		~ParticleSystem();

		void init(int MaxParticles);

		void begin();

		void draw(glm::vec4 destRect, ColourRGBA8 color);

		void end();

		void render();

	private:
		int m_maxParticles;
		GLuint vao;
		GLuint b_vbo;
		GLuint p_vbo;
		GLuint c_vbo;
		std::vector<Posit3D> m_ppb;
		std::vector<ColourRGBA8> m_color;
	};
}

